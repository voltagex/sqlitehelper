from distutils.core import setup

setup(
    name='sqlitehelper',
    version='0.001',
    packages=['sqlitehelper'],
    url='https://github.com/voltagex/sqlitehelper',
    license='MIT',
    author='Adam Baxter',
    author_email='voltagex@voltagex.org',
    description='A tiny helper for sqlite3'
)
