import sqlite3


class SQLiteHelper(sqlite3.Connection):
    def __init__(self, database, require_foreign_keys=True):
        super(SQLiteHelper, self).__init__(database)
        self.row_factory = sqlite3.Row

        self.execute('PRAGMA foreign_keys = ON')
        has_foreign_key_support = self.execute('PRAGMA foreign_keys').fetchone()[0]
        if not (has_foreign_key_support and require_foreign_keys):
            raise NotImplementedError("SQLite doesn't seem to support foreign keys")

    def number_of_rows(self, table_name):
        # can't use sqlite parameters here, hopefully terminating the statement is enough
        return self.execute('SELECT COUNT(*) FROM {0};'.format(table_name)).fetchone()[0]

    def table_exists(self, table_name):
        return len(self.execute("SELECT name FROM sqlite_master WHERE type='table' AND name=?",
                                [table_name, ]).fetchall()) > 0
